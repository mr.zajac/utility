/*
 * luhn.h
 *
 * Created by Michał Zając on 06-12-2017
 * This file is distributed under MIT license
 */
#ifndef _LUHN_H_
#define _LUHN_H_

char calc_luhn_digit(char *in);
bool luhn_correct(char *in);

#endif // _LUHN_H__
