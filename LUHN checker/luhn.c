/*
 * luhn.c
 *
 * Created by Michał Zając on 06-12-2017
 * This file is distributed under MIT license
 */
#include <string.h>
#include <stdbool.h>
#include "luhn.h"

char calc_luhn_digit(char *in)
{
    int len = strlen(in);
    int sum = 0;
    int tmp = 0;

    for (int i = len - 1; i >= 0; i--) {
        tmp = in[i] - 0x30;

        if (i % 2 != len % 2) 
            tmp = 2 * tmp; //multiply by weight 2

        if (tmp >= 10)
            tmp = tmp - 9; //same as adding first and seccond digits because max(tmp) == 18

        sum += tmp;
    }

    tmp = sum % 10;
    if (tmp != 0)
        tmp = 10 - tmp;

    return tmp;
}

bool luhn_correct(char *in)
{
    int len = strlen(in);
    int sum = 0;
    int tmp = 0;

    for (int i = len - 1; i >= 0; i--) {
        tmp = in[i] - 0x30;

        if (i % 2 == len % 2) 
            tmp = 2 * tmp; //multiply by weight 2

        if (tmp >= 10)
            tmp = tmp - 9; //same as adding first and seccond digits because max(tmp) == 18

        sum += tmp;
    }

    return sum % 10 == 0;
}

