/*
 * main.c
 *
 * Created by Michał Zając on 06-12-2017
 * This file is distributed under MIT license
 */
#include <stdio.h>
#include <stdbool.h>
#include "luhn.h"

int main() 
{
    printf("luhn(92480) = %d            [ %s ]\n", calc_luhn_digit("92480"), calc_luhn_digit("92480") == 3 ? "OK" : "NOT OK");
    printf("luhn(599040000020521) = %d  [ %s ]\n", calc_luhn_digit("599040000020521"), calc_luhn_digit("599040000020521") == 4 ? "OK" : "NOT OK");
    printf("luhn(4992739871) = %d       [ %s ]\n", calc_luhn_digit("4992739871"), calc_luhn_digit("4992739871") == 6 ? "OK" : "NOT OK");
    printf("luhn(123456781234567) = %d  [ %s ]\n", calc_luhn_digit("123456781234567"), calc_luhn_digit("123456781234567") == 0 ? "OK" : "NOT OK");
    printf("luhn(590000000000026) = %d  [ %s ]\n", calc_luhn_digit("590000000000026"), calc_luhn_digit("590000000000026") == 5 ? "OK" : "NOT OK");

    printf("luhn_correct(924803)          [ %s ]\n", luhn_correct("924803") ? "OK" : "INVALID LUHN");
    printf("luhn_correct(924804)          [ %s ]\n", luhn_correct("924804") ? "OK" : "INVALID LUHN");
    printf("luhn_correct(49927398716)     [ %s ]\n", luhn_correct("49927398716") ? "OK" : "INVALID LUHN");
    printf("luhn_correct(49927398717)     [ %s ]\n", luhn_correct("49927398717") ? "OK" : "INVALID LUHN");
    printf("luhn_correct(1234567812345678)[ %s ]\n", luhn_correct("1234567812345678") ? "OK" : "INVALID LUHN");
    printf("luhn_correct(1234567812345670)[ %s ]\n", luhn_correct("1234567812345670") ? "OK" : "INVALID LUHN");

    return 0;
}
