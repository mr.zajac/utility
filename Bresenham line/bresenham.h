/*
 * bresenham.h
 *
 * Created by Michał Zając on 07-12-2018
 * This file is distributed under MIT license
 */
#ifndef _BRESENHAM_H_
#define _BRESENHAM_H_
#include <stdbool.h>

int get_last_x();
int get_last_y();
bool line_bresenham(int y1, int x1, int y2, int x2, bool (*f_cb)(int, int));

#endif // _BRESENHAM_H_

