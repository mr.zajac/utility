/*
 * bresenham.c
 *
 * Created by Michał Zając on 07-12-2018
 * This file is distributed under MIT license
 */

/*
 * Description: 
 *      Implementation of the Bresenham line algorithm.
 */
#include <math.h> // fabs
#include "bresenham.h"

static int internal_x = 0;
static int internal_y = 0;

/*
 * Make a next step in the Bresenham algorithm changinh the y, x and err values.
 */
static void bresenham_step(int *y, int *x, float *err, int dy, int dx, float d_err)
{
    *x += dx; // move in the X axis (dx can be set to 0 meaning we have reached our destination, and we don't need to move in X axis)
    *err += d_err; // add error

    if (*err > 0.5) {
        *y += dy; // move in the Y axis (dy can be set to 0)
        *err -= 1;
    }
}

/*
 * Modify the [dx, dy] "direction vector" which is used to calculate the next 
 * step. This function can set the delta to 0 if we reached our destination.
 */
static void bresenham_vector(int *delta, int cur_pt, int dest)
{
    if (cur_pt == dest) 
        *delta = 0; // destiny in this coordinate is reached
}

/*
 * Function to get the last X cord. that was processed by the algorithm.
 */ 
int get_last_x()
{
    return internal_x;
}

/*
 * Function to get the last Y cord. that was processed by the algorithm.
 */ 
int get_last_y()
{
    return internal_y;
}

/*
 * Move from (y1, x1) to (y2, x2) calling f_cb() callback function each
 * step we make. This function will exit if the callback function returns 
 * false in any moment.
 */
bool line_bresenham(int y1, int x1, int y2, int x2, bool (*f_cb)(int, int))
{
    int dy = y2 > y1 ? 1 : -1; // this is the direction in which we are moving on the Y axis
    int dx = x2 > x1 ? 1 : -1; // this is the direction in which we are moving on the X axis
    int cur_y = y1;
    int cur_x = x1;
    float delta_err = fabs((float)(y2 - y1) / (float)(x2 - x1));
    float error = 0;

    while (true) {
        /* update internal x and y postion for referance if there was a collision*/
        internal_x = cur_x;
        internal_y = cur_y;
        
        bresenham_vector(&dx, cur_x, x2); // check if we don't have to set dx to 0
        bresenham_vector(&dy, cur_y, y2); // check if we don't have to set dy to 0

        if (!f_cb(cur_y, cur_x)) // apply the callback function
            return false; // line of sight blocked
        else if (dx == 0 && dy == 0)
            return true; // destination reached
        else
            bresenham_step(&cur_y, &cur_x, &error, dy, dx, delta_err);
    } 
}

