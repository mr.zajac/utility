#include <stdio.h>
#include "bresenham.h"

#define MAP_H 5
#define MAP_W 16

char map[MAP_H][MAP_W] = {
    "###############",
    "#A............#",
    "#....#........#",
    "#.......B.....#",
    "###############",
};

void print_map()
{
    for (int i = 0; i < MAP_H; i++) {
        for (int j = 0; j < MAP_W; j++) {
            putchar(map[i][j]);
        }
        putchar('\n');
    }
}

bool bresenham_callback_inSight(int y, int x)
{
    if (map[y][x] == '#') {
        map[y][x] = 'X';
        return false;
    } else {
        map[y][x] = '*';
        return true;
    }
}

bool bresenham_callback_blocked(int y, int x)
{
    map[y][x] = 'X';
    return true;
}

int main()
{
    int from_y = 1;
    int from_x = 1;
    int to_y = 3;
    int to_x = 8;

    /* Draw legend */
    printf("Drawing a line from point A (%d, %d) to point B (%d, %d)\n", from_y, from_x, to_y, to_x);
    printf("# - walls, . - empty space, * - in sight, X - not in sight/blocked\n");
    printf("Orginal map:\n");
    print_map();
    printf("\n");

    if (!line_bresenham(from_y, from_x, to_y, to_x, bresenham_callback_inSight)) {
       /* 
        * The line of sight is blocked from this point, check if it the dest. 
        * and if not the proceed drawing the line but but 'X' and not '*'
        */
        if (get_last_y() != to_y || get_last_x() != to_x) {
            printf("Colision detected in (%d, %d)\n", get_last_y(), get_last_x());
            line_bresenham(get_last_y(), get_last_x(), to_y, to_x, bresenham_callback_blocked);
        }
    }
    printf("\nMap with line of sight:\n");
    print_map();
    return 0;
}
