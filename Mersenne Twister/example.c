#include <time.h>
#include <stdio.h>
#include "mt19937.h"

int main()
{
    unsigned int r1;

    /* Initialize Mersenne Twister with random seed */
    init_mt19937(time(NULL));

    for (int i = 0; i < 70; i++) {
        r1 = rand_mt();
        printf("Random %02d: %u\n", i, r1);
    }

    return 0;
}
