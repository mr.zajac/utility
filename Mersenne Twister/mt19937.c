/*
 * mt19937.c
 *
 * Created by Michał Zając on 07-12-2018
 * This file is distributed under MIT license
 */

/* 
 * Description: 
 *      Implementation of the Mersenne Twister algorithm for generating 
 *      pseudo random numbers.
 */
#include <stdint.h>
#include "mt19937.h"

#define SEED_ARRAY_SIZE 624

static uint32_t seed_array[SEED_ARRAY_SIZE];
static int arr_idx = 0; // array index

/*
 * Initialize the seed_array with new seed. This needs to be called before
 * the program can generate a random number.
 */
void init_mt19937(unsigned int seed)
{
    uint32_t xor_val; // variable to hold the XOR value of seed_array[i] and seed_array[i] >> 30

    seed_array[0] = seed;

    for (int i = 0; i < SEED_ARRAY_SIZE - 1; i++) { 
        xor_val = seed_array[i] ^ (seed_array[i] >> 30);        // from i value in the array
        seed_array[i + 1] = (0x6c078965 * xor_val) + (i + 1);   // write the new i + 1 seed value
    }
}

/*
 * Generate random values based on the seed array.
 */
static void gen_new_array() 
{
    int modulo = SEED_ARRAY_SIZE;
    uint32_t bit_32;
    uint32_t bit_31_to_1;
    uint32_t bit_sum;

    for (int i = 0; i < SEED_ARRAY_SIZE; i++) {
        bit_32 = seed_array[i] & 0x80000000;
        bit_31_to_1 = seed_array[(i + 1) % modulo] & 0x7fffffff;  // modulo is used so the index 623 + 1 (value out of array) will be replaced with 0
        bit_sum = bit_32 + bit_31_to_1;

        seed_array[i] = seed_array[(i + 397) % modulo] ^ (bit_sum >> 1);

        if (1 == bit_sum % 2) // bit_sum is odd
            seed_array[i] = seed_array[i] ^ 0x9908b0df;
    }
}

/* 
 * Returns the next random number calculated from the seed_array. If there are
 * no more seeds in the array it calls gen_new_array() function automaticly.
 *
 * The return value should be in the range [0, (2^32) - 1].
 */
static uint32_t extract_rand_num()
{
    uint32_t r_num;

    if (0 == arr_idx)  // if we used all the seed in the array then generate a new seed array
        gen_new_array();

    /* get the seed from the array */
    r_num = seed_array[arr_idx];

    /* Using that seed generate a new random number. */
    r_num = r_num ^ (r_num >> 11);
    r_num = r_num ^ ((r_num << 7) & 0x9d2c5680);
    r_num = r_num ^ ((r_num << 15) & 0xefc60000);
    r_num = r_num ^ (r_num >> 18);

    arr_idx = (arr_idx + 1) % SEED_ARRAY_SIZE; //move the index to the next element

    return r_num;
}

/*
 * A function to call the internal extract_rand_num() and cast the return value 
 * to unsigned int.
 */
unsigned int rand_mt()
{
    return (unsigned int)extract_rand_num();
}

