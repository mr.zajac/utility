/*
 * mt19937.h
 *
 * Created by Michał Zając on 07-12-2018
 * This file is distributed under MIT license
 *
 */
#ifndef _MT19937_H_
#define _MT19937_H_

void init_mt19937(unsigned int seed);
unsigned int rand_mt();

#endif // _MT19937_H_

