/*
 * s_graph.c
 *
 * Created by Michał Zając on 05-11-2018
 * This file is distributed under MIT license
 */

/*
 * Simple graph structure
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * Simple structure to code the edges(arrows) of the graph. 
 */
struct edge {
    int from;
    int to;
};

/* 
 * Structure to store the information about the graph. Additionaly to 
 * storying vertices (V) and edges (E) it keaps track of how many 
 * vertices and edges are in the graph. Each edge e = (v_1, v_2) is 
 * stored as two arrows (a_1 = (v_1, v_2) and a_2 = (v_2, v_1)). 
 */
struct graph {
    int* V;
    int v_cnt;
    int v_max;
    struct edge* E;
    int e_cnt; 
    int e_max;
};

/*
 * Creates a empty graphs. This function allocates memory for 100 vertices and
 * 100 edges. 
 */
struct graph* graph_init()
{
    struct graph* G = malloc(sizeof(struct graph));
    
    if (NULL != G) {
        G->V = malloc(100 * sizeof(int));
        if (NULL == G->V) {
            free(G);
            return NULL;
        }
        G->v_max = 100;
        G->v_cnt = 0;

        G->E = malloc(100 * sizeof(struct edge));
        if (NULL == G->E) {
            free(G->V);
            free(G);
            return NULL;
        }
        G->e_max = 100;
        G->e_cnt = 0;
    }

    return G;
}

/*
 * Free memory and destroy the target graph. Changes 'target' variable 
 * to point to NULL.
 */
void graph_destroy(struct graph** target)
{
    struct graph* G = *target;

    if (NULL != G) {
        if (NULL != G->V)
            free(G->V);

        if (NULL != G->E)
            free(G->E);

        free(G);
        *target = NULL;
    }
}

/* 
 * Verify if a given vertex is in the graph. 
 */
bool is_vertex(struct graph* G, int v)
{
    if (NULL != G) {
        for (int i = 0; i < G->v_cnt; i++)
            if (G->V[i] == v)
                return true;
    }

    return false;
}

/* 
 * Adds a new vertex to the graph. the function first checks if the vertex
 * is already in our graph. If needed it allso allocates more memory to 
 * store the new vertex. 
 */
int add_vertex(struct graph* G, int v)
{
    if (NULL != G) {
        if (is_vertex(G, v))
            return -1;

        if (G->v_cnt >= G->v_max) {
            printf("Realloc V\n");
            int* tmp = realloc(G->V, (G->v_max + 100) * sizeof(int));
            if (NULL != tmp) {
                printf("Realloc V OK!\n");
                G->V = tmp;
                G->v_max += 100;
            }
        }

        if (G->v_cnt < G->v_max) {
            G->V[G->v_cnt] = v;
            G->v_cnt++;

            return 0;
        }
    }

    return -1;
}

/* 
 * Print all the vertices af a given graph.
 */
void print_vertices(struct graph* G)
{
    if (NULL != G) {
        printf("V = {");
        for (int i = 0; i < G->v_cnt; i++)
            printf("%d, ", G->V[i]);
        printf("}\n");
    }
}

/* 
 * Prints all arrows (one way edges) in a given graph.
 */
void print_arrows(struct graph* G)
{
    if (NULL != G) {
        printf("E = {"); 
        for (int i = 0; i < G->e_cnt; i++)
            printf("(%d, %d), ", G->E[i].from, G->E[i].to);
        printf("}\n");
    }
}

/* 
 * Tries to print all the edges. Assums the set V is a set of arrows (one way
 * edges) and that arrow a_1 = (v_1, v_2) and a_2 = (v_2, v_1) are next to 
 * each and other in the array E of graph G. 
 */
void print_edges(struct graph* G)
{
    if (NULL != G) {
        printf("E = {"); 
        for (int i = 0; i < G->e_cnt; i += 2)
            printf("(%d, %d), ", G->E[i].from, G->E[i].to);
        printf("}\n");
    }
}

/*
 * Verify is a given graph contains a arrow (from, to).
 */
bool is_arrow(struct graph* G, int from, int to)
{
    if (NULL != G) {
        for (int i = 0; i < G->e_cnt; i++)
            if (G->E[i].from == from && G->E[i].to == to)
                return true;
    }

    return false;
}

/* 
 * Adds a arrow (one wat edge) to a given graph.
 */
int add_arrow(struct graph* G, int from, int to)
{
    if (NULL != G) {
        if (!is_vertex(G, from) || !is_vertex(G, to))
            return -1;

        if (from == to)
            return -1;

        if (G->e_cnt >= G->e_max) {
            printf("Realloc E\n"); 
            struct edge* tmp = realloc(G->E, (G->e_max + 100) * sizeof(struct edge));
            if (NULL != tmp) {
                printf("Realloc E OK!\n");
                G->E = tmp;
                G->e_max += 100;
            }
        }

        if (G->e_cnt < G->e_max) {
            G->E[G->e_cnt].from = from;
            G->E[G->e_cnt].to = to;
            G->e_cnt++;

            return 0;
        }
    }

    return -1;
}

/* 
 * Adds a edge to a graph calling add_arrow() twice. 
 */
int add_edge(struct graph* G, int from, int to)
{
    return add_arrow(G, from, to) == 0 ? add_arrow(G, to, from) : -1;
}

int main()
{
    struct graph* G = graph_init();
    printf("Graph %p created\n", G);

    print_vertices(G);
    printf("Adding vertices...\n");
    add_vertex(G, 0);
    add_vertex(G, 1);
    add_vertex(G, 3);
    for (int i = 6; i < 12; i++)
        add_vertex(G, i);
    print_vertices(G);

    printf("Is 1 in G? %s\n", is_vertex(G, 1) ? "true" : "false");
    printf("Is 2 in G? %s\n", is_vertex(G, 2) ? "true" : "false");
    printf("Is 3 in G? %s\n", is_vertex(G, 3) ? "true" : "false");

    print_edges(G); 
    printf("Adding edges...\n");
    add_edge(G, 0, 1);
    add_edge(G, 0, 3);
    for (int i = 1; i < 6; i++)
        add_edge(G, i, 2 * i);
    add_edge(G, 6, 8);
    add_edge(G, 6, 7);
    add_edge(G, 7, 9);
    add_edge(G, 7, 10);
    add_edge(G, 11, 10);
    add_edge(G, 1, 10);

    print_edges(G); 

    printf("Is (0, 1) in G? %s\n", is_arrow(G, 0, 1) ? "true" : "false");
    printf("Is (3, 1) in G? %s\n", is_arrow(G, 3, 1) ? "true" : "false");
    printf("Is (3, 4) in G? %s\n", is_arrow(G, 3, 4) ? "true" : "false");

    
    printf("Destroy graph %p\n", G);
    graph_destroy(&G);
    if (NULL == G)
        printf("Graph destroyed\n");

    return 0;
}

